
public class comparatorComparable implements taxable {
	String name ;
	double income;
	double expenses;

	
	public comparatorComparable (String name , double income,  double expenses){
		this.name = name ;
		this.income = income;
		this.expenses = expenses;
	}
	
	

	@Override
	public double getTax() {
		double tax = 0;
		double profit = income-expenses;
		tax = profit*0.3;
		return tax;
	}
	
	public String toString() {
		return "Name[name="+this.name+", income="+this.income+", expenses"+this.expenses+"]";
	}

}
