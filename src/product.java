
public class product implements taxable,Comparable<product> {
	String name ;
	double price;
	
	public product (String name , double price){
		this.name = name ;
		this.price = price;
	}
	
	@Override
	public int compareTo(product other) {
		if (this.price < other.price ) { return -1; }
		if (this.price > other.price ) { return 1;  }
		return 0;
		}


	@Override
	public double getTax() {
		double tax = 0;
		tax = price *0.07;
		return tax;
	}
	public String toString() {
		return "Product[name="+this.name+", price="+this.price;
	}
	

}
