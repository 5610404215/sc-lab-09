import java.util.Comparator;


public class earningComparator implements Comparator {

	@Override
	public int compare(Object o1, Object o2) {
		double income1 = ((company)o1).getIncome();
		double income2 = ((company)o2).getIncome();
		if (income1 > income2) return 1;
		if (income1 < income2) return -1;
		return 0;
	}

}
