import java.util.Comparator;


public class comparator2 implements Comparator {

	@Override
	public int compare(Object a1, Object a2) {
		double expense1 = ((comparatorComparable)a1).getTax();
		double expense2 = ((comparatorComparable)a2).getTax();
		if (expense1 > expense2) return 1;
		if (expense1 < expense2) return -1;
		return 0;
	}

}


