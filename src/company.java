
public class company implements taxable {
	String name ;
	double income;
	double expenses;

	
	public company (String name , double income,  double expenses){
		this.name = name ;
		this.income = income;
		this.expenses = expenses;
	}
	
	

	@Override
	public double getTax() {
		double tax = 0;
		double profit = income-expenses;
		tax = profit*0.3;
		return tax;
	}
	public double getIncome(){
		return income;
	}
	public double getExpenses(){
		return expenses;
	}
	
	public String toString() {
		return "Name[name="+this.name+", income="+this.income+", expenses"+this.expenses+"]";
	}
}
