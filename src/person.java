
public class person implements Measurable,Comparable<person> {
	String name ;
	double salary;
	
	public person (String name , double salary){
		this.name = name ;
		this.salary = salary;
	}
	
	
	@Override
	public int compareTo(person other) {
		if (this.salary < other.salary ) { return -1; }
		if (this.salary > other.salary ) { return 1;  }
		return 0;
		}

	@Override
	public double getMeasure() {
		
		return salary;
	}
	
	public String toString() {
		return "NAME[name="+this.name+", salary="+this.salary;
	}
	
	
}
