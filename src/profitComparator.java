
import java.util.Comparator;


public class profitComparator implements Comparator {

	@Override
	public int compare(Object b1, Object b2) {
		double profit1 = ((company)b1).getTax();
		double profit2 = ((company)b2).getTax();
		if (profit1 > profit2) return 1;
		if (profit1 < profit2) return -1;
		return 0;
	}

}
