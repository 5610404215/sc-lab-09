
import java.util.Comparator;


public class expenseComparator implements Comparator {

	@Override
	public int compare(Object a1, Object a2) {
		double expense1 = ((company)a1).getExpenses();
		double expense2 = ((company)a2).getExpenses();
		if (expense1 > expense2) return 1;
		if (expense1 < expense2) return -1;
		return 0;
	}

}
