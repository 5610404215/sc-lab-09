import java.util.ArrayList;
import java.util.Collections;


public class test {
	public static void main(String[] args) {
		
		System.out.println("////////////////////////1.1/////////////////////////");
		ArrayList<person> persons = new ArrayList<person>();
		persons.add(new person("Kerkkong", 50000));
		persons.add(new person("Dang", 20000));
		persons.add(new person("Bam",60000));
		
		
		System.out.println("---before sort salary");
		for (person c : persons) 
			System.out.println(c);

		Collections.sort(persons);
		
		System.out.println("\n---after sort salary");
		for (person c : persons) 
			System.out.println(c);
		
		System.out.println("////////////////////////1.2/////////////////////////");
		ArrayList<product> prices = new ArrayList<product>();
		prices.add(new product("water", 10));
		prices.add(new product("greentea", 20));
		prices.add(new product("dim-sum",21));
		
		System.out.println("---before sort prices");
		for (product c : prices) 
			System.out.println(c);

		Collections.sort(prices);
		
		System.out.println("\n---after sort prices");
		for (product c : prices) 
			System.out.println(c);
			System.out.println();
		
		
		System.out.println("////////////////////////1.3/////////////////////////");
		ArrayList<company> companies = new ArrayList<company>();
		companies.add(new company("Beam", 50000, 7387));
		companies.add(new company("In", 23800, 7314));
		companies.add(new company("First", 68000, 5312));
		companies.add(new company("Porsche", 8000, 5002));
		companies.add(new company("Apple", 18000, 16800));
		
		
		System.out.println("---- Before sort");
		for (company c : companies) {
			System.out.println(c);
		}
		
		Collections.sort(companies, new earningComparator());
		System.out.println("\n---- After sort with earning");
		for (company c : companies) {
			System.out.println(c);
		}
		
		Collections.sort(companies, new expenseComparator());
		System.out.println("\n---- After sort with expense");
		for (company c : companies) {
			System.out.println(c);
		}

		Collections.sort(companies, new profitComparator());
		System.out.println("\n---- After sort with profit");
		for (company c : companies) {
			System.out.println(c);
		}
		System.out.println();
		
		System.out.println("////////////////////////1.4/////////////////////////");
		ArrayList<comparatorComparable> comparatorComparables = new ArrayList<comparatorComparable>();
		comparatorComparables.add(new comparatorComparable("Kerkkong", 50000,5000));
		comparatorComparables.add(new comparatorComparable("Dang", 200000,180000));
		comparatorComparables.add(new comparatorComparable("Bam",60000,30000));
		comparator2 comparator2 = new comparator2();
		System.out.println("---before sort tax");
		for (comparatorComparable c : comparatorComparables) 
			System.out.println(c);

		Collections.sort(comparatorComparables,comparator2 );
		
		System.out.println("\n---after sort tax");
		for (comparatorComparable c : comparatorComparables) 
			System.out.println(c);
		

	}

}
